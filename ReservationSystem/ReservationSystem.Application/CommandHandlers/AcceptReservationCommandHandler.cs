﻿using AutoMapper;
using MediatR;
using ReservationSystem.Contracts.ApiModels.Responses;
using ReservationSystem.Contracts.Commands;
using ReservationSystem.Contracts.Queries;
using ReservationSystem.Domain.Models;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ReservationSystem.Application.CommandHandlers
{
    public class AcceptReservationCommandHandler : IRequestHandler<AcceptReservationCommand, ReservationDetailsResponse>
    {
        private readonly IMapper _mapper;
        private readonly IWriteOnlyRepository<Reservation> _repoReservation;

        public AcceptReservationCommandHandler(IMapper mapper, IWriteOnlyRepository<Reservation> repoReservation)
        {
            _mapper = mapper;
            _repoReservation = repoReservation;
        }

        public async Task<ReservationDetailsResponse> Handle(
            AcceptReservationCommand command,
            CancellationToken cancellationToken)
        {
            // TODO: Check Owner somewhere
            var result = await _repoReservation.UpdateAsync(command.ReservtionId, res =>
            {
                res.Status = Domain.Enums.ReservationStatuses.Accepted;
                return res;
            });

            return _mapper.Map<ReservationDetailsResponse>(result);
        }
    }
}
