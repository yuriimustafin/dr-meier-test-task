﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReservationSystem.Persistence.Entities
{
    // The database table was named with 'Restaurant-' prefix to avoid confusion with DB Tables
    [Table("RestaurantTables")]
    public class TableEntity : IEntityWithId
    {
        public Guid Id { get; set; }

        public string Location { get; set; }

        public int NumberOfSits { get; set; }

        [ForeignKey("RestaurantEntity")]
        public Guid RestaurantId { get; set; }

        [ForeignKey("TableId")]
        public virtual IEnumerable<ReservationEntity> Reservations { get; set; }
    }
}
