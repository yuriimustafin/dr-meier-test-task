﻿using System;

namespace ReservationSystem.Contracts.ApiModels.Requests
{
    public class BookTableRequest
    {
        public Guid RestaurantId { get; set; }

        public Guid TableId { get; set; }

        public string Email { get; set; }

        public DateTime BookedFrom { get; set; }

        public DateTime BookedTo { get; set; }

        public int NumberOfVisitors { get; set; }

    }
}
