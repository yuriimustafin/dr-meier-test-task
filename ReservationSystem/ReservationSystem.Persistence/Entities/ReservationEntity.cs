﻿using ReservationSystem.Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReservationSystem.Persistence.Entities
{
    [Table("Reservations")]
    public class ReservationEntity : IEntityWithId
    {
        public Guid Id { get; set; }

        [ForeignKey("RestaurantEntity")]
        public Guid RestaurantId { get; set; }

        public RestaurantEntity RestaurantEntity { get; set; }

        [ForeignKey("TableEntity")]
        public Guid TableId { get; set; }

        public TableEntity TableEntity { get; set; }

        public ReservationStatuses Status { get; set; }

        public DateTime ReservedFrom { get; set; }

        public DateTime ReservedTo { get; set; }

        public int NumberOfVisitors { get; set; }

        public string Email { get; set; }
    }
}
