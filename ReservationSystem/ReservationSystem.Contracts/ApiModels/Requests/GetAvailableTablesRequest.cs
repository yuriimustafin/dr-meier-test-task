﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReservationSystem.Contracts.ApiModels.Requests
{
    public class GetAvailableTablesRequest
    {
        public Guid RestaurantId { get; set; }

        public DateTime? AvailableFrom { get; set; }

        public DateTime? AvailableTo { get; set; }

        public int NumberOfVisitors { get; set; }

    }
}
