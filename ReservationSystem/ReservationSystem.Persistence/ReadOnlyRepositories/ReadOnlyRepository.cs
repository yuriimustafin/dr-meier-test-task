﻿using AutoMapper;
using AutoMapper.Extensions.ExpressionMapping;
using Microsoft.EntityFrameworkCore;
using ReservationSystem.Application;
using ReservationSystem.Persistence.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ReservationSystem.Persistence
{
    public class ReadOnlyRepository<TDomain, TEntity>
        : AbstractRepository<TDomain, TEntity>, IReadOnlyRepository<TDomain>
        where TEntity : class, IEntityWithId
        where TDomain : class
    {

        public ReadOnlyRepository(ApplicationContext context, IMapper mapper) : base(context, mapper)
        { }

        public async Task<IEnumerable<TDomain>> GetAllWhereAsync(
            Expression<Func<TDomain, bool>> predicate, 
            params Expression<Func<TDomain, object>>[] includeProperties)
        {
            IQueryable<TEntity> queryable = _context.Set<TEntity>();

            if (predicate != null)
            {
                var entityPredicate = _mapper.Map<Expression<Func<TEntity, bool>>>(predicate);
                //var entityPredicate = _mapper.MapExpressionAsInclude<Expression<Func<TEntity, bool>>>(predicate);
                queryable = queryable.Where(entityPredicate);
            }

            queryable = ApplyInclude(queryable, includeProperties);

            var result = await queryable.ToListAsync();

            return _mapper.Map<IEnumerable<TDomain>>(result);
        }

    }
}
