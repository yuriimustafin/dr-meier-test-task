﻿using AutoMapper;
using MediatR;
using ReservationSystem.Contracts.ApiModels.Responses;
using ReservationSystem.Contracts.Queries;
using ReservationSystem.Domain.Models;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ReservationSystem.Application.QueryHandlers
{
    public class GetFilteredReservationsQueryHandler
        : IRequestHandler<GetFilteredReservationsQuery, IEnumerable<ReservationItemResponse>>
    {
        private readonly IReadOnlyRepository<Reservation> _repo;
        private readonly IMapper _mapper;

        public GetFilteredReservationsQueryHandler(IReadOnlyRepository<Reservation> repo, IMapper mapper)
        {
            _repo = repo;
            _mapper = mapper;
        }

        public async Task<IEnumerable<ReservationItemResponse>> Handle(
            GetFilteredReservationsQuery query,
            CancellationToken cancellationToken)
        {
            // TODO: Remove workaround (issue: https://github.com/AutoMapper/AutoMapper/issues/2016)
            var workAround = new 
            { 
                TableIdHasValue = query.TableId.HasValue,
                RestaurantIdHasValue = query.RestaurantId.HasValue,
                TableIdValue = query.TableId.HasValue ? query.TableId.Value : Guid.Empty,
                RestaurantIdValue = query.RestaurantId.HasValue ? query.RestaurantId.Value : Guid.Empty,
                ReservedFromHasValue = query.ReservedFrom.HasValue,
                ReservedToHasValue = query.ReservedTo.HasValue,
                ReservedFromValue = query.ReservedFrom.HasValue ? query.ReservedFrom.Value : new DateTime(),
                ReservedToValue = query.ReservedTo.HasValue ? query.ReservedTo.Value : new DateTime()
            };

            var result = await _repo.GetAllWhereAsync(res =>
                (!workAround.TableIdHasValue || res.TableId == workAround.TableIdValue)
                && (!workAround.RestaurantIdHasValue || res.RestaurantId == workAround.RestaurantIdValue)
                && (!(workAround.ReservedFromHasValue && workAround.ReservedToHasValue)
                    || (workAround.ReservedFromValue <= res.ReservedPeriod.PeriodTo 
                        && workAround.ReservedToValue >= res.ReservedPeriod.PeriodFrom))
                && (String.IsNullOrEmpty(query.Email) || res.Email == query.Email)
            );

            return _mapper.Map<IEnumerable<ReservationItemResponse>>(result);
        }

        // TODO: Use it when workaround below would be fixed
        private bool IsSatisfyRequestedPeriod(
            DateTime? requestedFrom, 
            DateTime? requestedTo, 
            DateTime reservedFrom, 
            DateTime reservedTo)
        {
            return !AreBothTimeForPeriodSet(requestedFrom, reservedTo)
                    || (requestedFrom <= reservedTo && requestedTo >= reservedFrom);
        }

        private bool AreBothTimeForPeriodSet(DateTime? requestedFrom, DateTime? requestedTo) => requestedFrom.HasValue && requestedTo.HasValue;
    }
}
