﻿using ReservationSystem.Domain.Enums;
using ReservationSystem.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReservationSystem.Domain.Models
{
    public class Reservation
    {
        public Guid Id { get; set; }

        public Guid RestaurantId { get; set; }

        public RestaurantAggregate Restaurant { get; set; }

        public Guid TableId { get; set; }

        public Table Table { get; set; }

        public ReservationStatuses Status { get; set; } = ReservationStatuses.Created;

        public PeriodValueObj ReservedPeriod { get; set; }

        public int NumberOfVisitors { get; set; }

        public string Email { get; set; }
    }
}
