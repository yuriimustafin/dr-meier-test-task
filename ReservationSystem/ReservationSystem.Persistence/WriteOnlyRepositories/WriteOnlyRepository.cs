﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ReservationSystem.Application;
using ReservationSystem.Persistence.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ReservationSystem.Persistence
{
    public abstract class WriteOnlyRepository<TDomain, TEntity> 
        : AbstractRepository<TDomain, TEntity>, IWriteOnlyRepository<TDomain>
        where TEntity : class, IEntityWithId
        where TDomain : class
    {
        public WriteOnlyRepository(ApplicationContext context, IMapper mapper) : base (context, mapper)
        {
        }

        public async Task<TDomain> AddAsync(TDomain domainModel, CancellationToken cancellationToken = default)
        {
            var entity = _mapper.Map<TEntity>(domainModel);

            entity = (await _context.Set<TEntity>().AddAsync(entity))
                        .Entity;
            await _context.SaveChangesAsync(cancellationToken);

            return _mapper.Map<TDomain>(entity);
        }

        public async Task<TDomain> UpdateAsync(TDomain domainModel, CancellationToken cancellationToken = default)
        {
            var entity = _mapper.Map<TEntity>(domainModel);

            return await UpdateEntityAsync(entity, cancellationToken);
        }

        public async Task<TDomain> UpdateAsync(
            Guid id,
            Func<TDomain, TDomain> domainChangingPredicate,
            CancellationToken cancellationToken = default)
        {
            var entity = await _context.Set<TEntity>().FirstOrDefaultAsync(x => x.Id == id);
            var domainModel = _mapper.Map<TDomain>(entity);

            domainModel = domainChangingPredicate(domainModel);

            _mapper.Map(domainModel, entity);

            return await UpdateEntityAsync(entity, cancellationToken);
        }

        public async Task DeleteAsync(TDomain domainModel, CancellationToken cancellationToken = default)
        {
            var entity = _mapper.Map<TEntity>(domainModel);

            await DeleteEntityAsync(entity);
        }

        public async Task DeleteAsync(Guid id, CancellationToken cancellationToken = default)
        {
            var entity = await _context.Set<TEntity>().FirstOrDefaultAsync(x => x.Id == id);

            await DeleteEntityAsync(entity);
        }

        private async Task<TDomain> UpdateEntityAsync(TEntity entity, CancellationToken cancellationToken = default)
        {
            _context.Entry(entity).State = EntityState.Modified;
            await _context.SaveChangesAsync(cancellationToken);

            return _mapper.Map<TDomain>(entity);
        }

        private async Task DeleteEntityAsync(TEntity entity, CancellationToken cancellationToken = default)
        {
            _context.Set<TEntity>().Remove(entity);
            await _context.SaveChangesAsync(cancellationToken);
        }

    }
}
