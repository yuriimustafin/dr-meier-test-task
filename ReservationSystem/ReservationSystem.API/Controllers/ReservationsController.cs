﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ReservationSystem.Contracts.ApiModels.Requests;
using ReservationSystem.Contracts.Commands;
using ReservationSystem.Contracts.Queries;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ReservationSystem.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReservationsController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;

        public ReservationsController(IMediator mediator, IMapper mapper)
        {
            _mediator = mediator;
            _mapper = mapper;
        }

        /// <summary>
        /// Get Filtered Reservations 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetFilteredReservations([FromQuery] GetFilteredReservationsRequest request)
        {
            var query = _mapper.Map<GetFilteredReservationsQuery>(request);
            return Ok(await _mediator.Send(query));
        }

        [HttpPost("/available")]
        public async Task<IActionResult> GetAvailableTables([FromBody] GetAvailableTablesRequest request)
        {
            var query = _mapper.Map<GetAvailableTablesQuery>(request);
            return Ok(await _mediator.Send(query));
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetReservationById(Guid id)
        {
            var query = new GetReservationByIdQuery(id);
            return Ok(await _mediator.Send(query));
        }

        [HttpPost]
        public async Task<IActionResult> BookTable([FromBody] BookTableRequest request)
        {
            var command = _mapper.Map<BookTableCommand>(request);
            return Ok(await _mediator.Send(command));
        }


        [HttpPatch("{id}")]
        [Authorize(Policy = "ApiScope")]
        public async Task<IActionResult> AcceptReservation(Guid id, [FromBody] AcceptReservationRequest request)
        {
            var command = _mapper.Map<AcceptReservationCommand>(request);

            command.ReservtionId = id;
            command.OwnerId = new Guid((User.Identity as ClaimsIdentity)?.FindFirst(ClaimTypes.NameIdentifier).Value);

            return Ok(await _mediator.Send(command));
        }

    }
}
