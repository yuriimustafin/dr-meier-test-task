﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ReservationSystem.Contracts.ApiModels.Requests
{
    public class GetFilteredReservationsRequest
    {
        public Guid? TableId { get; set; }

        [Required]
        public Guid RestaurantId { get; set; }

        public DateTime? ReservedFrom { get; set; }

        public DateTime? ReservedTo { get; set; }

        public string Email { get; set; }
    }
}
