﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReservationSystem.Domain.Enums
{
    // We could avoid code duplication by adding a Common project. 
    // In a real-world project, I would consider this depending on the quantity of duplicated code.
    public enum ReservationStatuses
    {
        Created,
        Pending,
        Accepted,
        Canceled
    }
}
