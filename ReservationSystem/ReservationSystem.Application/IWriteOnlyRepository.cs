﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ReservationSystem.Application
{
    public interface IWriteOnlyRepository<TDomain>: IRepository<TDomain>
    {
        Task<TDomain> AddAsync(TDomain domainModel, CancellationToken cancellationToken = default);

        Task<TDomain> UpdateAsync(TDomain domainModel, CancellationToken cancellationToken = default);

        Task<TDomain> UpdateAsync(
            Guid id, 
            Func<TDomain,TDomain> domainChangingPredicate, 
            CancellationToken cancellationToken = default);

        Task DeleteAsync(Guid id, CancellationToken cancellationToken = default);

        Task DeleteAsync(TDomain domainModel, CancellationToken cancellationToken = default);
    }
}
