﻿using AutoMapper;
using ReservationSystem.Domain.Models;
using ReservationSystem.Persistence.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReservationSystem.Persistence.WriteOnlyRepositories
{
    public class ReservationWriteRepository : WriteOnlyRepository<Reservation, ReservationEntity>
    {
        public ReservationWriteRepository(ApplicationContext context, IMapper mapper) : base(context, mapper)
        {
        }
    }
}
