﻿using AutoMapper;
using MediatR;
using ReservationSystem.Contracts.ApiModels.Responses;
using ReservationSystem.Contracts.Commands;
using ReservationSystem.Contracts.Queries;
using ReservationSystem.Domain.Models;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ReservationSystem.Application.CommandHandlers
{
    public class BookTableCommandHandler : IRequestHandler<BookTableCommand, ReservationDetailsResponse>
    {
        private readonly IMapper _mapper;
        private readonly IMediator _mediator;
        private readonly IWriteOnlyRepository<Reservation> _repoReservation;

        public BookTableCommandHandler(
            IMapper mapper, 
            IMediator mediator, 
            IWriteOnlyRepository<Reservation> repoReservation)
        {
            _mapper = mapper;
            _mediator = mediator;
            _repoReservation = repoReservation;
        }

        public async Task<ReservationDetailsResponse> Handle(
            BookTableCommand command,
            CancellationToken cancellationToken)
        {
            // TODO: Consider to move this logic from the handler
            var availableTables = await _mediator.Send(
                new GetAvailableTablesQuery(command.RestaurantId, command.BookedFrom, command.BookedTo, command.NumberOfVisitors));

            if(!availableTables.Any(at => at.Id == command.TableId) 
                || command.NumberOfVisitors > availableTables.FirstOrDefault(at => at.Id == command.TableId).NumberOfSits)
            {
                throw new ArgumentException();
            }

            var reservation = _mapper.Map<Reservation>(command);
            reservation = await _repoReservation.AddAsync(reservation);
            return _mapper.Map<ReservationDetailsResponse>(reservation);
        }
    }
}
