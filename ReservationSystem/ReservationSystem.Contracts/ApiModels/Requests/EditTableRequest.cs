﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReservationSystem.Contracts.ApiModels.Requests
{
    public class EditTableRequest
    {
        public Guid RestarauntId { get; set; }

        public Guid TableId { get; set; }

        public string Location { get; set; }

        public int NumberOfSits { get; set; }
    }
}
