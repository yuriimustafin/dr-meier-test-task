﻿using ReservationSystem.Contracts.Enums;
using System;

namespace ReservationSystem.Contracts.ApiModels.Responses
{
    public class ReservationDetailsResponse
    {
        public Guid Id { get; set; }

        public Guid RestaurantId { get; set; }
        public RestaurantItemResponse Restaurant { get; set; }

        public Guid TableId { get; set; }

        public TableItemResponse Table { get; set; }

        public ReservationStatuses Status { get; set; }

        public DateTime ReservedFrom { get; set; }

        public DateTime ReservedTo { get; set; }

        public string Email { get; set; }

    }
}
