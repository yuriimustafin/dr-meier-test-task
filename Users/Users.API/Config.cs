﻿using IdentityServer4;
using IdentityServer4.Models;
using System.Collections.Generic;

namespace Users.API
{
    public class Config
    {

        public static IEnumerable<IdentityResource> IdentityResources =>
            new List<IdentityResource>
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
            };


        public static IEnumerable<ApiScope> ApiScopes =>
            new List<ApiScope>
            {
                new ApiScope("ReservationSystem.API", "ReservationSystem API")
            };

        public static IEnumerable<Client> Clients =>
            new List<Client>
            {
                // machine to machine client
                new Client
                {
                    ClientId = "ReservationSystem",
                    ClientSecrets = { new Secret("secret".Sha256()) },

                    // where to redirect to after login
                    RedirectUris = { 
                        "https://localhost:6001/signin-oidc",
                        "https://oauth.pstmn.io/v1/callback"
                    },

                    AllowedGrantTypes = {
                        GrantType.Implicit
                    },

                    AllowAccessTokensViaBrowser = true,

                    // scopes that client has access to
                    AllowedScopes = {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        "ReservationSystem.API"
                    }
                }
            };
    }
}
