﻿using MediatR;
using ReservationSystem.Contracts.Commands;
using ReservationSystem.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ReservationSystem.Application.CommandHandlers
{
    public class CreateRestaurantCommandHandler: IRequestHandler<CreateRestaurantCommand, Guid>
    {
        private readonly IWriteOnlyRepository<RestaurantAggregate> _repo;

        public CreateRestaurantCommandHandler(IWriteOnlyRepository<RestaurantAggregate> repo)
        {
            _repo = repo;
        }

        public async Task<Guid> Handle(CreateRestaurantCommand command, CancellationToken cancellationToken)
        {
            var restaurant = new RestaurantAggregate(command.RestaurantName, command.OwnerId);
            return (await _repo.AddAsync(restaurant, cancellationToken)).Id.Value;
        }
    }
}
