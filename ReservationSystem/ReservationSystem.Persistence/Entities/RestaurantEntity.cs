﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ReservationSystem.Persistence.Entities
{
    [Table("Restaurants")]
    public class RestaurantEntity: IEntityWithId
    {
        public Guid Id { get; set; }

        public string RestaurantName { get; set; }

        [ForeignKey("RestaurantId")]
        public virtual IEnumerable<TableEntity> Tables { get; set; }

        [ForeignKey("RestaurantId")]
        public virtual IEnumerable<ReservationEntity> Reservations { get; set; }

        public Guid OwnerId { get; set; }
    }
}
