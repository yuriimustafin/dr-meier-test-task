﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReservationSystem.Domain.Models
{
    public class Table
    {
        public Guid Id { get; set; }

        public string Location { get; set; }

        public int NumberOfSits { get; set; }

        public Guid RestaurantId { get; set; }

        public RestaurantAggregate Restaurant { get; set; }

    }
}
