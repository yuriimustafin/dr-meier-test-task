﻿using System;

namespace ReservationSystem.Contracts.ApiModels.Requests
{
    public class CreateRestaurantRequest
    {
        public string RestaurantName { get; set; }

    }
}
