﻿using ReservationSystem.Contracts.Enums;
using System;

namespace ReservationSystem.Contracts.ApiModels.Responses
{
    public class ReservationItemResponse
    {
        public Guid Id { get; set; }

        public Guid TableId { get; set; }

        public ReservationStatuses Status { get; set; }

        public DateTime ReservedFrom { get; set; }

        public DateTime ReservedTo { get; set; }
    }
}
