﻿using System;
using System.Collections.Generic;

namespace ReservationSystem.Contracts.ApiModels.Responses
{
    public class TableDetailsResponse
    {
        public Guid Id { get; set;  }

        public string Location { get; set; }

        public int NumberOfSits { get; set; }

        public IEnumerable<ReservationItemResponse> Reservations { get; set; }
    }
}
