﻿using AutoMapper;
using AutoMapper.Extensions.ExpressionMapping;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ReservationSystem.Application;
using ReservationSystem.Domain.Models;
using ReservationSystem.Persistence;
using ReservationSystem.Persistence.ReadOnlyRepositories;
using ReservationSystem.Persistence.WriteOnlyRepositories;
using System;

namespace ReservationSystem.CompositionRoot
{
    public static class StartupExtensions
    {
        public static void AddDependencies(this IServiceCollection services)
        {
            //services.AddSingleton(typeof(IRepository<>), typeof(InMemoryRepo<>));
            services.AddTransient<IWriteOnlyRepository<RestaurantAggregate>, RestaurantWriteRepository>();
            services.AddTransient<IWriteOnlyRepository<Table>, TableWriteRepository>();
            services.AddTransient<IWriteOnlyRepository<Reservation>, ReservationWriteRepository>();
            services.AddTransient<IReadOnlyRepository<RestaurantAggregate>, RestaurantReadRepository>();
            services.AddTransient<IReadOnlyRepository<Table>, TableReadRepository>();
            services.AddTransient<IReadOnlyRepository<Reservation>, ReservationReadRepository>();
            services.AddMediatR(AppDomain.CurrentDomain.GetAssemblies());

            // Auto Mapper Configurations
            var mapperConfig = new MapperConfiguration(mc =>
            {
                mc.AddExpressionMapping();
                mc.AddProfile(new MappingProfile());
            });

            IMapper mapper = mapperConfig.CreateMapper();
            services.AddSingleton(mapper);
        }

        public static void AddPersistenceServices(this IServiceCollection services, IConfiguration configuration)
        {
            // TODO: Fix issue with docker and db connection (!!!)
            services.AddDbContext<ApplicationContext>(options => {
                options.UseNpgsql(configuration.GetConnectionString("DefaultConnection"));
            });
        }
    }
}
