﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReservationSystem.Contracts.ApiModels.Requests
{
    public class CreateTableRequest
    {
        public Guid RestaurantId { get; set; }

        public string Location { get; set; }

        public int NumberOfSits { get; set; }
    }
}
