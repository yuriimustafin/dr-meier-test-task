﻿using System;
using System.Collections.Generic;
using MediatR;
using ReservationSystem.Contracts.ApiModels.Responses;

namespace ReservationSystem.Contracts.Queries
{
    public class GetAvailableTablesQuery: IRequest<IEnumerable<TableItemResponse>>
    {
        public Guid RestaurantId { get; set; }

        public DateTime? AvailableFrom { get; set; }

        public DateTime? AvailableTo { get; set; }

        public int NumberOfVisitors { get; set; }

        public GetAvailableTablesQuery(Guid restaurantId, DateTime? availableFrom, DateTime? availableTo, int numberOfVisitors)
        {
            RestaurantId = restaurantId;
            AvailableFrom = availableFrom;
            AvailableTo = availableTo;
            NumberOfVisitors = numberOfVisitors;
        }
    }
}
