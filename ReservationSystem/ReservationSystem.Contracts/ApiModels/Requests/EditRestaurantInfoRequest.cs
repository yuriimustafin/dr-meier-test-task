﻿using System;

namespace ReservationSystem.Contracts.ApiModels.Requests
{
    public class EditRestaurantInfoRequest
    {
        public string NewRestaurantName { get; set; }
    }
}
