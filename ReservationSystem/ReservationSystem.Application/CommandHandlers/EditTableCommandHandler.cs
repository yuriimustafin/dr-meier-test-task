﻿using AutoMapper;
using MediatR;
using ReservationSystem.Contracts.ApiModels.Responses;
using ReservationSystem.Contracts.Commands;
using ReservationSystem.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ReservationSystem.Application.CommandHandlers
{
    public class EditTableCommandHandler : IRequestHandler<EditTableCommand, TableDetailsResponse>
    {
        private readonly IWriteOnlyRepository<Table> _repo;
        private readonly IMapper _mapper;

        public EditTableCommandHandler(IWriteOnlyRepository<Table> repo, IMapper mapper)
        {
            _repo = repo;
            _mapper = mapper;
        }

        public async Task<TableDetailsResponse> Handle(
            EditTableCommand command,
            CancellationToken cancellationToken)
        {

            var result = await _repo.UpdateAsync(command.TableId,
                table =>
                {
                    table.Location = command.Location ?? table.Location;
                    table.NumberOfSits = command.NumberOfSits == 0 
                                        ? table.NumberOfSits 
                                        : command.NumberOfSits;
                    return table;
                });

            return _mapper.Map<TableDetailsResponse>(result);
        }
    }
}
