﻿using AutoMapper;
using ReservationSystem.Domain.Models;
using ReservationSystem.Persistence.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReservationSystem.Persistence.ReadOnlyRepositories
{
    public class TableReadRepository : ReadOnlyRepository<Table, RestaurantEntity>
    {
        public TableReadRepository(IMapper mapper, ApplicationContext context) : base(context, mapper)
        { }
    }
}
