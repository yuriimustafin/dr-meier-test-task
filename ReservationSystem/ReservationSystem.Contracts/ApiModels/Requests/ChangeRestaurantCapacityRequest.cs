﻿using System;
using System.Collections.Generic;

namespace ReservationSystem.Contracts.ApiModels.Requests
{
    public class ChangeRestaurantCapacityRequest
    {
        public Guid RestaurantId { get; set; }

        public IEnumerable<Guid> TablesToRemove { get; set; }

        public IEnumerable<CreateTableRequest> TablesToAdd { get; set; }

    }
}
