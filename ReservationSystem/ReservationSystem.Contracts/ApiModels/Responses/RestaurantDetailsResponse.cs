﻿using System;
using System.Collections.Generic;

namespace ReservationSystem.Contracts.ApiModels.Responses
{
    public class RestaurantDetailsResponse
    {
        public Guid Id { get; set; }

        public string RestaurantName { get; set; }

        public IEnumerable<TableItemResponse> Tables { get; set; }

        public IEnumerable<ReservationItemResponse> Reservations { get; set; }
    }
}
