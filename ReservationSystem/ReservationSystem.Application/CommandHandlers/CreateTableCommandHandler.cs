﻿using AutoMapper;
using MediatR;
using ReservationSystem.Contracts.ApiModels.Responses;
using ReservationSystem.Contracts.Commands;
using ReservationSystem.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ReservationSystem.Application.CommandHandlers
{
    public class CreateTableCommandHandler
        : IRequestHandler<CreateTableCommand, TableDetailsResponse>
    {
        private readonly IWriteOnlyRepository<Table> _repo;
        private readonly IMapper _mapper;

        public CreateTableCommandHandler(IWriteOnlyRepository<Table> tableRepo, IMapper mapper)
        {
            _repo = tableRepo;
            _mapper = mapper;
        }

        public async Task<TableDetailsResponse> Handle(
            CreateTableCommand command,
            CancellationToken cancellationToken)
        {
            var newTable = _mapper.Map<Table>(command);

            var result = await _repo.AddAsync(newTable);

            return _mapper.Map<TableDetailsResponse>(result);
        }
    }
}
