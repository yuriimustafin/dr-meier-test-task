﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReservationSystem.Domain.Models
{
    public class RestaurantAggregate
    {
        public Guid? Id { get; set; }

        public string RestaurantName { get; set; }

        public IEnumerable<Table> Tables { get; set; }

        public IEnumerable<Reservation> Reservations { get; set; }

        public int Capacity { 
            get
            {
                return Tables.Count();
            } 
        }

        public Guid OwnerId { get; set; }

        public RestaurantAggregate() : this(null, Guid.NewGuid())
        {
        }

        public RestaurantAggregate(string restaurantName, Guid ownerId) : this(Guid.NewGuid(), restaurantName, ownerId)
        {

        }

        public RestaurantAggregate(Guid? id, string restaurantName, Guid ownerId)
        {
            if (ownerId == default(Guid) || ownerId == Guid.Empty)
            {
                throw new ArgumentNullException(nameof(ownerId));
            }

            Id = id;
            RestaurantName = restaurantName;
            OwnerId = ownerId;
        }
    }
}
