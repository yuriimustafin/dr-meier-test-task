﻿using Microsoft.EntityFrameworkCore;
using ReservationSystem.Persistence.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReservationSystem.Persistence
{
    public class ApplicationContext: DbContext
    {
        public DbSet<RestaurantEntity> Restaurants { get; set; }
        public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options)
        {
        }
    }
}
