﻿using System;

namespace ReservationSystem.Contracts.ApiModels.Responses
{
    public class TableItemResponse
    {
        public Guid Id { get; set;  }

        public string Location { get; set; }

        public int NumberOfSits { get; set; }
    }
}
