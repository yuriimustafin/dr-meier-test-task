﻿using System;

namespace ReservationSystem.Contracts.ApiModels.Responses
{
    public class RestaurantItemResponse
    {
        public Guid Id { get; set; }

        public string RestaurantName { get; set; }

        public int Capacity { get; set; }

    }
}
