﻿using AutoMapper;
using MediatR;
using ReservationSystem.Contracts.ApiModels.Responses;
using ReservationSystem.Contracts.Queries;
using ReservationSystem.Domain.Models;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ReservationSystem.Application.QueryHandlers
{
    public class GetRestaurantListQueryHandler
        : IRequestHandler<GetRestaurantListQuery, IEnumerable<RestaurantDetailsResponse>>
    {
        private readonly IReadOnlyRepository<RestaurantAggregate> _repo;
        private readonly IMapper _mapper;

        public GetRestaurantListQueryHandler(IReadOnlyRepository<RestaurantAggregate> repo, IMapper mapper)
        {
            _repo = repo;
            _mapper = mapper;
        }

        public async Task<IEnumerable<RestaurantDetailsResponse>> Handle(
            GetRestaurantListQuery query,
            CancellationToken cancellationToken)
        {
            var result = await _repo
                .GetAllWhereAsync(rest => rest.OwnerId == query.OwnerId, r => r.Tables, r => r.Reservations);

            return _mapper.Map<IEnumerable<RestaurantDetailsResponse>>(result);
        }
    }
}
