﻿using System;
using MediatR;
using ReservationSystem.Contracts.ApiModels.Responses;

namespace ReservationSystem.Contracts.Queries
{
    public class GetReservationByIdQuery: IRequest<ReservationDetailsResponse>
    {
        public Guid ReservationId { get; set; }

        public GetReservationByIdQuery(Guid reservationId)
        {
            ReservationId = reservationId;
        }
    }
}
