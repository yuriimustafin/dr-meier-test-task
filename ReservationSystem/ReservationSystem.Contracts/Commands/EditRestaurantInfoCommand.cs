﻿using System;
using MediatR;
using ReservationSystem.Contracts.ApiModels.Responses;

namespace ReservationSystem.Contracts.Commands
{
    public class EditRestaurantInfoCommand: IRequest<RestaurantDetailsResponse>
    {
        public Guid RestaurantId { get; set; }

        public string NewRestaurantName { get; set; }

        public Guid OwnerId { get; set; }
    }
}
