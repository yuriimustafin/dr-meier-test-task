﻿using MediatR;
using System;

namespace ReservationSystem.Contracts.Commands
{
    public class CreateRestaurantCommand: IRequest<Guid>
    {
        public string RestaurantName { get; set; }

        public Guid OwnerId { get; set; }

    }
}
