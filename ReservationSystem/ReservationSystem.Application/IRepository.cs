﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ReservationSystem.Application
{
    public interface IRepository<TDomain>
    {
        Task<TDomain> GetByIdAsync(Guid Id, params Expression<Func<TDomain, object>>[] includeProperties);
    }
}
