﻿using MediatR;
using ReservationSystem.Contracts.ApiModels.Responses;
using System;

namespace ReservationSystem.Contracts.Commands
{
    public class BookTableCommand : IRequest<ReservationDetailsResponse>
    {
        public Guid RestaurantId { get; set; }

        public Guid TableId { get; set; }

        public string Email { get; set; }

        public DateTime BookedFrom { get; set; }

        public DateTime BookedTo { get; set; }

        public int NumberOfVisitors { get; set; }

    }
}
