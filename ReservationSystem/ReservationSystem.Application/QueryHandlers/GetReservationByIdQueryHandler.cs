﻿using AutoMapper;
using MediatR;
using ReservationSystem.Contracts.ApiModels.Responses;
using ReservationSystem.Contracts.Queries;
using ReservationSystem.Domain.Models;
using System.Threading;
using System.Threading.Tasks;

namespace ReservationSystem.Application.QueryHandlers
{
    public class GetReservationByIdQueryHandler
        : IRequestHandler<GetReservationByIdQuery, ReservationDetailsResponse>
    {
        private readonly IReadOnlyRepository<Reservation> _repo;
        private readonly IMapper _mapper;

        public GetReservationByIdQueryHandler(IReadOnlyRepository<Reservation> repo, IMapper mapper)
        {
            _repo = repo;
            _mapper = mapper;
        }

        public async Task<ReservationDetailsResponse> Handle(
            GetReservationByIdQuery query,
            CancellationToken cancellationToken)
        {
            var result = await _repo.GetByIdAsync(query.ReservationId, r => r.Table, r => r.Restaurant);
            return _mapper.Map<ReservationDetailsResponse>(result);
        }
    }
}
