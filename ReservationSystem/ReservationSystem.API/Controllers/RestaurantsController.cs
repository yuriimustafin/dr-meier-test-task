﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ReservationSystem.Contracts.ApiModels.Requests;
using ReservationSystem.Contracts.ApiModels.Responses;
using ReservationSystem.Contracts.Commands;
using ReservationSystem.Contracts.Queries;

namespace ReservationSystem.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize(Policy = "ApiScope")]
    [ApiController]
    public class RestaurantsController : ControllerBase
    {

        private readonly IMediator _mediator;
        private readonly IMapper _mapper;

        public RestaurantsController(IMediator mediator, IMapper mapper)
        {
            _mediator = mediator;
            _mapper = mapper;
        }

        /// <summary>
        /// GetRestaurantList - get all restaurants for specific owner
        /// </summary>
        /// <returns></returns>
        // GET: api/<RestaurantsController>
        [HttpGet]
        public async Task<IActionResult> GetRestaurantList()
        {
            var query = new GetRestaurantListQuery(GetUserId());
            return Ok(await _mediator.Send(query));
        }

        // GET api/<RestaurantsController>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetRestaurantById(Guid id)
        {
            var query = new GetRestaurantByIdQuery(id);
            return Ok(await _mediator.Send(query));
        }

        // POST api/<RestaurantsController>
        [HttpPost]
        public async Task<IActionResult> CreateRestaurant([FromBody] CreateRestaurantRequest request)
        {
            var command = _mapper.Map<CreateRestaurantCommand>(request);
            command.OwnerId = GetUserId();
            
            return Ok(await _mediator.Send(command));
        }

        // PUT api/<RestaurantsController>/5
        [HttpPut("{id}")]
        public async Task<IActionResult> EditRestaurantInfo(Guid id, [FromBody] EditRestaurantInfoRequest request)
        {

            var command = _mapper.Map<EditRestaurantInfoCommand>(request);

            command.RestaurantId = id;
            command.OwnerId = GetUserId();
            
            return Ok(await _mediator.Send(command));
        }


        // PATCH api/<RestaurantsController>/5
        [HttpPatch("{id}")]
        public async Task<IActionResult> ChangeRestaurantCapacity(Guid id, [FromBody] ChangeRestaurantCapacityRequest request)
        {
            var command = _mapper.Map<ChangeRestaurantCapacityCommand>(request);

            command.RestaurantId = id;
            command.OwnerId = GetUserId();

            return Ok(await _mediator.Send(command));
        }


        // POST api/<RestaurantsController>
        [HttpPost("{restarauntId}/tables")]
        public async Task<IActionResult> CreateTable(Guid restarauntId, [FromBody] CreateTableRequest request)
        {
            var command = _mapper.Map<CreateTableCommand>(request);

            command.RestaurantId = restarauntId;
            command.OwnerId = GetUserId();
            
            return Ok(await _mediator.Send(command));
        }


        // POST api/<RestaurantsController>
        [HttpPut("{restarauntId}/tables/{tableId}")]
        public async Task<IActionResult> EditTable(
            Guid restarauntId, 
            Guid tableId, 
            [FromBody] EditTableRequest request)
        {
            var command = _mapper.Map<EditTableCommand>(request);

            command.TableId = tableId;
            command.OwnerId = GetUserId();
            
            return Ok(await _mediator.Send(command));
        }

        private Guid GetUserId() => new Guid((User.Identity as ClaimsIdentity)?.FindFirst(ClaimTypes.NameIdentifier).Value);

    }
}
