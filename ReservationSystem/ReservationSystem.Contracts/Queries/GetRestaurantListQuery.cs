﻿using MediatR;
using ReservationSystem.Contracts.ApiModels.Responses;
using System;
using System.Collections.Generic;

namespace ReservationSystem.Contracts.Queries
{
    public class GetRestaurantListQuery: IRequest<IEnumerable<RestaurantDetailsResponse>>
    {
        public Guid OwnerId { get; set; }

        public GetRestaurantListQuery(Guid ownerId)
        {
            OwnerId = ownerId;
        }
    }
}
