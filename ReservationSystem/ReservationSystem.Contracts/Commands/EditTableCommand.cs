﻿using System;
using MediatR;
using ReservationSystem.Contracts.ApiModels.Responses;

namespace ReservationSystem.Contracts.Commands
{
    public class EditTableCommand : IRequest<TableDetailsResponse>
    {
        public Guid TableId { get; set; }

        public string Location { get; set; }

        public int NumberOfSits { get; set; }

        public Guid OwnerId { get; set; }
    }
}
