﻿using AutoMapper;
using MediatR;
using ReservationSystem.Contracts.ApiModels.Responses;
using ReservationSystem.Contracts.Commands;
using ReservationSystem.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ReservationSystem.Application.CommandHandlers
{
    public class EditRestaurantInfoCommandHandler : IRequestHandler<EditRestaurantInfoCommand, RestaurantDetailsResponse>
    {
        private readonly IWriteOnlyRepository<RestaurantAggregate> _repo;
        private readonly IMapper _mapper;

        public EditRestaurantInfoCommandHandler(IWriteOnlyRepository<RestaurantAggregate> repo, IMapper mapper)
        {
            _repo = repo;
            _mapper = mapper;
        }

        public async Task<RestaurantDetailsResponse> Handle(
            EditRestaurantInfoCommand command, 
            CancellationToken cancellationToken)
        {

            var result = await _repo.UpdateAsync(command.RestaurantId,
                x =>
                {
                    x.RestaurantName = command.NewRestaurantName;
                    return x;
                });

            return _mapper.Map<RestaurantDetailsResponse>(result);
        }
    }
}
