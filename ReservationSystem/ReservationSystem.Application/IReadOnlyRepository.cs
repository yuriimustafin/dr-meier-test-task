﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ReservationSystem.Application
{
    public interface IReadOnlyRepository<TDomain>: IRepository<TDomain>
    {
        Task<IEnumerable<TDomain>> GetAllWhereAsync(
            Expression<Func<TDomain, bool>> predicate, 
            params Expression<Func<TDomain, object>>[] includeProperties
            );

    }
}
