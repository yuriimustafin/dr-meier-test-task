﻿using System;
using System.Collections.Generic;
using MediatR;
using ReservationSystem.Contracts.ApiModels.Responses;

namespace ReservationSystem.Contracts.Commands
{
    public class ChangeRestaurantCapacityCommand : IRequest<RestaurantDetailsResponse>
    {
        public Guid RestaurantId { get; set; }

        public IEnumerable<Guid> TablesToRemove { get; set; }

        public IEnumerable<CreateTableCommand> TablesToAdd { get; set; }

        public Guid OwnerId { get; set; }

    }
}
