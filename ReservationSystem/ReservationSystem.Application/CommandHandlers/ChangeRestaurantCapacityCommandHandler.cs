﻿using AutoMapper;
using MediatR;
using ReservationSystem.Contracts.ApiModels.Responses;
using ReservationSystem.Contracts.Commands;
using ReservationSystem.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ReservationSystem.Application.CommandHandlers
{
    public class ChangeRestaurantCapacityCommandHandler 
        : IRequestHandler<ChangeRestaurantCapacityCommand, RestaurantDetailsResponse>
    {
        private readonly IWriteOnlyRepository<Table> _tableRepo;
        private readonly IWriteOnlyRepository<RestaurantAggregate> _restaurantRepo;
        private readonly IMapper _mapper;
        private readonly IMediator _mediator;

        public ChangeRestaurantCapacityCommandHandler(
            IWriteOnlyRepository<Table> tableRepo,
            IWriteOnlyRepository<RestaurantAggregate> restaurantRepo, 
            IMapper mapper,
            IMediator mediator)
        {
            _tableRepo = tableRepo;
            _mapper = mapper;
            _mediator = mediator;
            _restaurantRepo = restaurantRepo;
        }

        public async Task<RestaurantDetailsResponse> Handle(
            ChangeRestaurantCapacityCommand command,
            CancellationToken cancellationToken)
        {
            Task.WaitAll(command.TablesToRemove
                            .Select(id => _tableRepo.DeleteAsync(id))
                            .ToArray());

            Task.WaitAll(command.TablesToAdd
                            .Select(createCommand => _mediator.Send(createCommand))
                            .ToArray());

            // We could raise here a 'Get By Id' query, but if we would use a different db store 
            // or different schemas it would not work. IMO, it's better to consider not to return a value for commands.
            var result = await _restaurantRepo.GetByIdAsync(command.RestaurantId, x => x.Tables);
            return _mapper.Map<RestaurantDetailsResponse>(result);
        }
    }
}
