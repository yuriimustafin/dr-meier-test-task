﻿using AutoMapper;
using AutoMapper.Extensions.ExpressionMapping;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using ReservationSystem.Application;
using ReservationSystem.Persistence.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ReservationSystem.Persistence
{
    public class AbstractRepository<TDomain, TEntity> : IRepository<TDomain> 
        where TEntity : class, IEntityWithId
        where TDomain : class
    {
        protected readonly ApplicationContext _context;
        protected readonly IMapper _mapper;

        public AbstractRepository(ApplicationContext context, IMapper mapper)
        {
            _mapper = mapper;
            _context = context;
        }

        public async Task<TDomain> GetByIdAsync(Guid id, params Expression<Func<TDomain, object>>[] includeProperties)
        {
            IQueryable<TEntity> queryable = _context.Set<TEntity>();

            queryable = ApplyInclude(queryable, includeProperties);

            var entity = await queryable.FirstOrDefaultAsync(x => x.Id == id);
            return _mapper.Map<TDomain>(entity);
        }

        protected IQueryable<TEntity> ApplyInclude(
            IQueryable<TEntity> queryable,
            Expression<Func<TDomain, object>>[] includeProperties)
        {
            if (includeProperties == null || includeProperties.Length == 0)
                return queryable;

            foreach (var includeProperty in includeProperties)
            {
                var includeEntityProperty = _mapper.MapExpressionAsInclude<Expression<Func<TEntity, object>>>(includeProperty);
                queryable = queryable.Include(includeEntityProperty);
            }

            return queryable;
        }
    }
}
