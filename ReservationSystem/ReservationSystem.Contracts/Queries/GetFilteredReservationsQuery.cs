﻿using MediatR;
using ReservationSystem.Contracts.ApiModels.Responses;
using System;
using System.Collections.Generic;

namespace ReservationSystem.Contracts.Queries
{
    public class GetFilteredReservationsQuery: IRequest<IEnumerable<ReservationItemResponse>>
    {
        public Guid? TableId { get; set; }

        public Guid? RestaurantId { get; set; }

        public DateTime? ReservedFrom { get; set; }
        
        public DateTime? ReservedTo { get; set; }

        public string Email { get; set; }
    }
}
