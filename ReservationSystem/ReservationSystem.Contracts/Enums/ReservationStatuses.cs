﻿namespace ReservationSystem.Contracts.Enums
{
    public enum ReservationStatuses
    {
        Created,
        Pending,
        Accepted,
        Canceled
    }
}
