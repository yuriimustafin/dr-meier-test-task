﻿using AutoMapper;
using MediatR;
using ReservationSystem.Contracts.ApiModels.Responses;
using ReservationSystem.Contracts.Queries;
using ReservationSystem.Domain.Models;
using System.Threading;
using System.Threading.Tasks;

namespace ReservationSystem.Application.QueryHandlers
{
    public class GetRestaurantByIdQueryHandler
        : IRequestHandler<GetRestaurantByIdQuery, RestaurantDetailsResponse>
    {
        private readonly IReadOnlyRepository<RestaurantAggregate> _repo;
        private readonly IMapper _mapper;

        public GetRestaurantByIdQueryHandler(IReadOnlyRepository<RestaurantAggregate> restaurantRepo, IMapper mapper)
        {
            _repo = restaurantRepo;
            _mapper = mapper;
        }

        public async Task<RestaurantDetailsResponse> Handle(
            GetRestaurantByIdQuery query,
            CancellationToken cancellationToken)
        {
            var result = await _repo.GetByIdAsync(query.RestaurantId, r => r.Tables, r => r.Reservations);
            return _mapper.Map<RestaurantDetailsResponse>(result);
        }
    }
}
