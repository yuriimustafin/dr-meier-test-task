﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReservationSystem.Domain.ValueObjects
{
    public struct PeriodValueObj
    {
        public static int StartWorkingHours =8;
        public static int EndWorkingHours = 23;

        public DateTime PeriodFrom { get; set; }

        public DateTime PeriodTo { get; set; }

        public PeriodValueObj(DateTime? periodFrom = null, DateTime? periodTo = null)
        {
            if (!periodFrom.HasValue && !periodTo.HasValue)
                this = default;

            PeriodFrom = periodFrom.HasValue
                ? GetCorrectTimeFrom(periodFrom.Value)
                : GetCorrectTimeTo(periodTo.Value);

            PeriodTo = periodTo.HasValue
                ? GetCorrectTimeFrom(periodTo.Value)
                : GetCorrectTimeTo(periodFrom.Value);
        }

        private static DateTime GetCorrectTimeFrom(DateTime timeFrom)
        {
            if (timeFrom.Hour < PeriodValueObj.StartWorkingHours)
            {
                return timeFrom.Date.AddHours(PeriodValueObj.StartWorkingHours);
            }
            return timeFrom;
        }
        private static DateTime GetCorrectTimeTo(DateTime timeTo)
        {
            if (timeTo.Hour > PeriodValueObj.EndWorkingHours)
            {
                return timeTo.Date.AddHours(PeriodValueObj.EndWorkingHours);
            }
            return timeTo;
        }
    }
}
