﻿using AutoMapper;
using ReservationSystem.Domain.Models;
using ReservationSystem.Persistence.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReservationSystem.Persistence.WriteOnlyRepositories
{
    public class RestaurantWriteRepository : WriteOnlyRepository<RestaurantAggregate, RestaurantEntity>
    {
        public RestaurantWriteRepository(ApplicationContext context, IMapper mapper) : base(context, mapper)
        {
        }
    }
}
