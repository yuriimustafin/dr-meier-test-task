﻿using AutoMapper;
using ReservationSystem.Contracts.ApiModels.Requests;
using ReservationSystem.Contracts.ApiModels.Responses;
using ReservationSystem.Contracts.Commands;
using ReservationSystem.Contracts.Enums;
using ReservationSystem.Contracts.Queries;
using ReservationSystem.Domain.Models;
using ReservationSystem.Domain.ValueObjects;
using ReservationSystem.Persistence.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReservationSystem.CompositionRoot
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {

            CreateMap<Reservation, ReservationEntity>()
                .ForMember(dest => dest.TableEntity, opt => opt.MapFrom(src => src.Table))
                .ForMember(dest => dest.RestaurantEntity, opt => opt.MapFrom(src => src.Restaurant))
                .ForMember(dest => dest.ReservedFrom, opt => opt.MapFrom(src => src.ReservedPeriod.PeriodFrom))
                .ForMember(dest => dest.ReservedTo, opt => opt.MapFrom(src => src.ReservedPeriod.PeriodTo))
                .ReverseMap()
                .ForMember(dest => dest.ReservedPeriod,
                            opt => opt.MapFrom(src => new PeriodValueObj(src.ReservedFrom, src.ReservedTo))); ;
            CreateMap<RestaurantAggregate, RestaurantEntity>()
                .ReverseMap();
            CreateMap<Table, TableEntity>()
                .ReverseMap();
            

            CreateMap<AcceptReservationRequest, AcceptReservationCommand>()
                .ReverseMap();
            CreateMap<BookTableRequest, BookTableCommand>()
                .ReverseMap();
            CreateMap<ChangeRestaurantCapacityRequest, ChangeRestaurantCapacityCommand>()
                .ReverseMap();
            CreateMap<CreateRestaurantRequest, CreateRestaurantCommand>()
                .ReverseMap();
            CreateMap<CreateTableRequest, CreateTableCommand>()
                .ReverseMap();
            CreateMap<EditRestaurantInfoRequest, EditRestaurantInfoCommand>()
                .ReverseMap();
            CreateMap<EditTableRequest, EditTableCommand>()
                .ReverseMap();
            CreateMap<GetFilteredReservationsRequest, GetFilteredReservationsQuery>()
                .ReverseMap();
            CreateMap<GetAvailableTablesRequest, GetAvailableTablesQuery>()
                .ReverseMap();
            CreateMap<BookTableRequest, BookTableCommand>()
                .ReverseMap();
            CreateMap<AcceptReservationRequest, AcceptReservationCommand>()
                .ReverseMap();


            CreateMap<CreateTableCommand, Table>()
                .ReverseMap();
            CreateMap<BookTableCommand, Reservation>()
                .ForMember(dest => dest.ReservedPeriod, 
                            opt => opt.MapFrom(src => new PeriodValueObj(src.BookedFrom, src.BookedTo)))
                .ReverseMap();

            
            CreateMap<Table, TableDetailsResponse>()
                .ReverseMap();
            CreateMap<Table, TableItemResponse>()
                .ReverseMap();
            CreateMap<RestaurantAggregate, RestaurantDetailsResponse>()
                .ReverseMap();
            CreateMap<RestaurantAggregate, RestaurantItemResponse>()
                .ReverseMap();
            CreateMap<Reservation, ReservationDetailsResponse>()
                .ReverseMap()
                .ForMember(dest => dest.ReservedPeriod,
                            opt => opt.MapFrom(src => new PeriodValueObj(src.ReservedFrom, src.ReservedTo)));
            CreateMap<Reservation, ReservationItemResponse>()
                .ReverseMap()
                .ForMember(dest => dest.ReservedPeriod,
                            opt => opt.MapFrom(src => new PeriodValueObj(src.ReservedFrom, src.ReservedTo)));


        }
    }
}
