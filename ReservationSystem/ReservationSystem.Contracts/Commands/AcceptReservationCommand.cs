﻿using MediatR;
using ReservationSystem.Contracts.ApiModels.Responses;
using System;

namespace ReservationSystem.Contracts.Commands
{
    public class AcceptReservationCommand : IRequest<ReservationDetailsResponse>
    {
        public Guid ReservtionId { get; set; }

        public Guid OwnerId { get; set; }

    }
}
