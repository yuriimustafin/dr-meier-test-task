﻿using System;
using MediatR;
using ReservationSystem.Contracts.ApiModels.Responses;

namespace ReservationSystem.Contracts.Queries
{
    public class GetRestaurantByIdQuery: IRequest<RestaurantDetailsResponse>
    {
        public Guid RestaurantId { get; set; }

        public GetRestaurantByIdQuery(Guid restaurantId)
        {
            RestaurantId = restaurantId;
        }
    }
}
