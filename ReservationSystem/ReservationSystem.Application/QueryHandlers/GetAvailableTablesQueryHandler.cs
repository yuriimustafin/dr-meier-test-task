﻿using AutoMapper;
using MediatR;
using ReservationSystem.Contracts.ApiModels.Responses;
using ReservationSystem.Contracts.Queries;
using ReservationSystem.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ReservationSystem.Application.QueryHandlers
{
    public class GetAvailableTablesQueryHandler
        : IRequestHandler<GetAvailableTablesQuery, IEnumerable<TableItemResponse>>
    {
        private readonly IMapper _mapper;
        private readonly IReadOnlyRepository<RestaurantAggregate> _restaurantsRepo;
        private readonly IReadOnlyRepository<Reservation> _reservationsRepo;

        public GetAvailableTablesQueryHandler(
            IMapper mapper, 
            IReadOnlyRepository<RestaurantAggregate> restaurantsRepo, 
            IReadOnlyRepository<Reservation> reservationsRepo)
        {
            _mapper = mapper;
            _restaurantsRepo = restaurantsRepo;
            _reservationsRepo = reservationsRepo;
        }

        public async Task<IEnumerable<TableItemResponse>> Handle(
            GetAvailableTablesQuery query,
            CancellationToken cancellationToken)
        {
            var reservedDuringSpecifiedPeriod = await _reservationsRepo
                .GetAllWhereAsync(res => query.AvailableFrom <= res.ReservedPeriod.PeriodTo
                                        && query.AvailableTo >= res.ReservedPeriod.PeriodFrom);

            var restaurant = await _restaurantsRepo
                .GetByIdAsync(query.RestaurantId, rest => rest.Tables);

            var result = restaurant.Tables
                            .Where(t => t.NumberOfSits >= query.NumberOfVisitors
                                     && !reservedDuringSpecifiedPeriod
                                            .Select(reserve => reserve.TableId)
                                            .Contains(t.Id));

            return _mapper.Map<IEnumerable<TableItemResponse>>(result);
        }
    }
}
